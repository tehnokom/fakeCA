# CA

## информация к сведению

[Теория СА для начинающих](https://sysadmins.ru/topic238106.html)

"особенности" Хромиума

https://security.stackexchange.com/a/183973
https://stackoverflow.com/a/53826340

## создание сертификатов СА

```
make CA
```

в папке `out/` будут созданы файлы

```
out/rootCA.crt
out/rootCA.key
out/rootCA.srl
```

`rootCA.crt` добавлять в браузеры.

### для firefox

Скопировать адрес настроек [about:preferences#privacy](about:preferences#privacy) и открыть страницу настроек в браузере.

внизу -> кнопка **Просмотр сертификатов** -> вкладка **Центры сертификации** -> кнопка **Импортировать** -> нажать птичку **Доверять при идентификации веб-сайтов**

### для chromium

Скопировать адрес настроек [chrome://settings/certificates](chrome://settings/certificates) и открыть страницу настроек в браузере.

Перейти на закладку **Центры сертификации** (Authorities). Нажать кнопку **Импорт** (Import).

В открывшемся окне выбрать ранее скачанный сертификат.
В следующем открывшемся окне проставить все флажки.

Нажать кнопку **Ок**.

ниже описан процесс управления сертификатами через консоль при помощи `certutil`

## создание сертификата для сервера

```
make server SERVER=example.com
```

в папке `out/example.com` будет созданы файлы для сервера `example.com` (заменить на свой)

```
out/example.com/fullchain.pem
out/example.com/server.crt
out/example.com/server.csr
out/example.com/server.key
```

если не указывать `SERVER` используется `localhost`

## тестирование сертификата (только для сервера localhost)

запуск докер контейнера

```
docker-compose up
```

[https://localhost:8443/](https://localhost:8443/)

можно проверить curl\`ом

```
make test
```

### как удалить / добавить CA в FireFox / хромиум в линукс из консоли

поставить утилиту `certutil`

```
sudo apt install libnss3-tools
```

посмотреть список установленных сертификатов

```shell
# firefox
certutil -L -d sql:"/home/<user_name>/.mozilla/firefox/<profile_name>.default/"
# chromium-browser
certutil -L -d sql:"/home/<user_name>/.pki/nssdb/"
```

в конце будет что то вида:

```
Explicitly Distrusted DigiNotar PKIoverheid G2               ,,
Explicitly Distrust DigiNotar Root CA                        ,,
universo.pro - Tehnokom                                      CT,c,
```

удалить:

```shell
certutil -D -n "universo.pro - Tehnokom" -d sql:"/home/<user_name>/.mozilla/firefox/<profile_name>.default/"
```

добавить:

```shell
certutil -A -n "<authory_name>" -t "TC,," -d sql:"/home/<user_name>/.pki/nssdb/" -i <path/to/rootCA.crt>
```

`user_name` и `profile_name` заменить по вкусу

`authory_name` это в примере выше `universo.pro - Tehnokom`
